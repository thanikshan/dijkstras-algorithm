//package assignment3V2;


//class to store the intersection as x and y in the corresponding object

class Point {
	int x, y;
	// String name;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
		// this.name = name;
	}

	@Override
	public boolean equals(Object p) {
		// TODO Auto-generated method stub
		if (p instanceof Point) {
			return x == ((Point) p).x && y == ((Point) p).y;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return ("" + x + "-" + y).hashCode();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "[" + x + "," + y + "]";
	}
}