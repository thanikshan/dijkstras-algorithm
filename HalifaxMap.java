//package assignment3V2;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class HalifaxMap {
	
	//creating a hashset to store the points
	Set<Point> points = new HashSet<>();
	//Hashmap to store the paths between the points
	Map<Point, Set<Point>> paths = new HashMap<>();

	//method to store the points based on the user input.An instance of the class point will be created for it
	public Boolean newIntersection(int x, int y) {
		
		//return true or false based on the object inserted in the points set
		return points.add(new Point(x, y));
	}

	public Boolean defineRoad(int x1, int y1, int x2, int y2) {

		//create object of class point  
		Point a = new Point(x1, y1);
		Point b = new Point(x2, y2);
		//adding to the set if it is already present it will be ignored
		points.add(a);
		points.add(b);

		//creating a set to store the paths of the object a
		Set<Point> pathsFromA = paths.get(a);
		
		
		if (pathsFromA == null) {
			pathsFromA = new HashSet<>();
			paths.put(a, pathsFromA);
		}

		//adding adjacent path b to the paths of a
		boolean result = pathsFromA.add(b);
		//System.out.println(result);
        
		//creating a set to store the paths of the object b
		Set<Point> pathsFromB = paths.get(b);
		if (pathsFromB == null) {
			pathsFromB = new HashSet<>();
			paths.put(b, pathsFromB);
		}
		
		//adding adjacent path from point b to the point a since the map is bidirectional
		boolean result2=pathsFromB.add(a);
		//System.out.println(result2);

		return result || result2;
	}

	//method to find minimum distance from x1,y1 --> x2,y2
	void navigate(int x1, int y1, int x2, int y2) {
		Point startPoint = new Point(x1, y1);
		Point endPoint = new Point(x2, y2);
		
		//calling djikstra algorithm to find the shortest path from starting point to destination
		djikstraAlgorithm(points, paths, startPoint, endPoint);
	}
	
	private static void djikstraAlgorithm(Set<Point> points, Map<Point, Set<Point>> paths, Point startPoint,
			Point endPoint) {

		//creating sets to keep track of visited and unvisited points to find the shortest distance
		Set<Point> visited = new HashSet<>();
		Set<Point> unvisited = new HashSet<>();

		//hashmap to store the points and its corresponding cost and previous node
		Map<Point, DjikstraRow> djRows = new HashMap<>();
		djRows.put(startPoint, new DjikstraRow(0, startPoint));

		//adding all the points from the set points to the set unvisited
		unvisited.addAll(points);

		//loop until all the points processed and minimum cost to all the points from startPoint is determined
		while (!unvisited.isEmpty()) {
			int minCost = -1;
			Point nextPoint = null;
			for (Point p : unvisited) {
				if (djRows.containsKey(p) && djRows.get(p).cost != -1) {
					if (minCost == -1 || minCost > djRows.get(p).cost) {
						minCost = djRows.get(p).cost;
						nextPoint = p;
					}
				}
			}
			if (nextPoint == null && !visited.contains(endPoint)) {
				System.out.println("End node unreachable");
				return;
			}
			djikstra(nextPoint, djRows, paths, minCost);
			unvisited.remove(nextPoint);
			visited.add(nextPoint);
		}
		
		System.out.println("Min cost: " + djRows.get(endPoint).cost);
		Point prevPoint = djRows.get(endPoint).prev;
		String path =  prevPoint + " - " + endPoint;
		while (!prevPoint.equals(startPoint)) {
			prevPoint = djRows.get(prevPoint).prev;
			path = prevPoint + " - " + path;
		}
		System.out.println("Path: " + path);
	}

	private static void printDjikstraRows(Map<Point, DjikstraRow> djRows) {
		for (Point p : djRows.keySet()) {
			System.out.println(p + "\t" + djRows.get(p).cost + "\t" + djRows.get(p).prev);
		}
	}
    
	//method to store the minimum cost of the paths
	private static void djikstra(Point start, Map<Point, DjikstraRow> djRows, Map<Point, Set<Point>> paths,
			int minCost) {
		Set<Point> pathsFromPoint = paths.get(start);
		if (pathsFromPoint == null || pathsFromPoint.isEmpty()) {
			return;
		}
		for (Point p : pathsFromPoint) {
			Path path = new Path(start, p);
			int cost = path.cost + minCost;
			DjikstraRow djRow = djRows.get(p);
			if (djRow == null || cost < djRow.cost) {
				djRow = new DjikstraRow(cost, start);
				djRows.put(p, djRow);
			}
		}
	}
}

