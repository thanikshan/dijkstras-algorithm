//package assignment3V2;

//class that contains cost and its previous point

class DjikstraRow {
	int cost;
	Point prev;

	public DjikstraRow() {
		this.cost = -1;
	}

	public DjikstraRow(int cost, Point prev) {
		this.cost = cost;
		this.prev = prev;
	}
}