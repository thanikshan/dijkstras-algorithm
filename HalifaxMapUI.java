//package assignment3V2;

import java.util.Scanner;

public class HalifaxMapUI {
	public static void main(String[] args) {
		HalifaxMap h = new HalifaxMap();
		Scanner in = new Scanner(System.in);
		String userCommand = "", userArgument = "";
		int a, b;
		int x, y;

		do {
			// Find out what the user wants to do
			userCommand = in.next();
			if (userCommand.equals("add")) {
				System.out.println("add road points");
				a = in.nextInt();
				b = in.nextInt();
				boolean value=h.newIntersection(a, b);
				System.out.println(value);
			} else if (userCommand.equals("road")) {
				System.out.println("join road");
				a = in.nextInt();
				b = in.nextInt();
				x = in.nextInt();
				y = in.nextInt();
				boolean value2=h.defineRoad(a, b, x, y);
				System.out.println(value2);
			} else if (userCommand.equals("findpath")) {
				System.out.println("find road");
				a = in.nextInt();
				b = in.nextInt();
				x = in.nextInt();
				y = in.nextInt();
				h.navigate(a, b, x, y);
			}

			else {
				break;
			}
		} while (!userCommand.equalsIgnoreCase("quit"));

	}

}
