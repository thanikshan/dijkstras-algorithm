//package assignment3V2;

//class to define roads and store cost between each roads
class Path {
	Point start, end;
	int cost;
    //constructor to store the road with its cost
	public Path(Point start, Point end) {
		this.start = start;
		this.end = end;
		this.cost = (int) Math.sqrt((end.x - start.x) * (end.x - start.x) + (end.y - start.y) * (end.y - start.y));
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if (obj instanceof Path) {
			Path p = (Path) obj;
			return start.equals(p.start) && end.equals(p.end);
		}
		return false;
	}
}

